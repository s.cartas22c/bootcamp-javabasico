package com.example;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

        List<String>  nombres = new ArrayList<>();
        nombres.add("Madrid");
        nombres.add("Toledo");
        nombres.add("Sevilla");
        nombres.add("Cadiz");
        nombres.add("Alicante");
        nombres.add("Barcelona");

        String cadenaFinal = "";
        for(String nombre:nombres){

            cadenaFinal =  cadenaFinal + nombre + " ";

        }

        System.out.println(cadenaFinal);

    }
}