package com.example;

public class SmartWatch extends  SmartDevice{

    private boolean heartRateMonitors;
    private boolean sleepMonitor;
    private boolean messaging;
    private int internalMemory;

    public SmartWatch(){

    }


    public SmartWatch(int autonomyHours, String typeConnectivity, double weight, double price) {
        super(autonomyHours, typeConnectivity, weight, price);
    }

    public SmartWatch(boolean heartRateMonitors, boolean sleepMonitor, boolean messaging, int internalMemory) {
        this.heartRateMonitors = heartRateMonitors;
        this.sleepMonitor = sleepMonitor;
        this.messaging = messaging;
        this.internalMemory = internalMemory;
    }

    public SmartWatch(int autonomyHours, String typeConnectivity, double weight, double price, boolean heartRateMonitors, boolean sleepMonitor, boolean messaging, int internalMemory) {
        super(autonomyHours, typeConnectivity, weight, price);
        this.heartRateMonitors = heartRateMonitors;
        this.sleepMonitor = sleepMonitor;
        this.messaging = messaging;
        this.internalMemory = internalMemory;
    }

    public boolean isHeartRateMonitors() {
        return heartRateMonitors;
    }

    public void setHeartRateMonitors(boolean heartRateMonitors) {
        this.heartRateMonitors = heartRateMonitors;
    }

    public boolean isSleepMonitor() {
        return sleepMonitor;
    }

    public void setSleepMonitor(boolean sleepMonitor) {
        this.sleepMonitor = sleepMonitor;
    }

    public boolean isMessaging() {
        return messaging;
    }

    public void setMessaging(boolean messaging) {
        this.messaging = messaging;
    }

    public int getInternalMemory() {
        return internalMemory;
    }

    public void setInternalMemory(int internalMemory) {
        this.internalMemory = internalMemory;
    }
}
