package com.example;

public class SmartDevice {

    private int autonomyHours;
    private String typeConnectivity;
    private double weight;
    private double price;


    public SmartDevice() {
    }

    public SmartDevice(int autonomyHours, String typeConnectivity, double weight, double price) {
        this.autonomyHours = autonomyHours;
        this.typeConnectivity = typeConnectivity;
        this.weight = weight;
        this.price = price;
    }

    public int getAutonomyHours() {
        return autonomyHours;
    }

    public void setAutonomyHours(int autonomyHours) {
        this.autonomyHours = autonomyHours;
    }

    public String getTypeConnectivity() {
        return typeConnectivity;
    }

    public void setTypeConnectivity(String typeConnectivity) {
        this.typeConnectivity = typeConnectivity;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) {
        this.weight = weight;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
