package com.example;

public class SmartPhone extends SmartDevice{

    private int screenSize;
    private int cameraPixels;
    private boolean dualSIM;
    private boolean faceUnlock;


    public SmartPhone() {
    }

    public SmartPhone(int autonomyHours, String typeConnectivity, double weight, double price) {
        super(autonomyHours, typeConnectivity, weight, price);
    }

    public SmartPhone(int screenSize, int cameraPixels, boolean dualSIM, boolean faceUnlock) {
        this.screenSize = screenSize;
        this.cameraPixels = cameraPixels;
        this.dualSIM = dualSIM;
        this.faceUnlock = faceUnlock;
    }

    public SmartPhone(int autonomyHours, String typeConnectivity, double weight, double price, int screenSize, int cameraPixels, boolean dualSIM, boolean faceUnlock) {
        super(autonomyHours, typeConnectivity, weight, price);
        this.screenSize = screenSize;
        this.cameraPixels = cameraPixels;
        this.dualSIM = dualSIM;
        this.faceUnlock = faceUnlock;
    }

    public int getScreenSize() {
        return screenSize;
    }

    public void setScreenSize(int screenSize) {
        this.screenSize = screenSize;
    }

    public int getCameraPixels() {
        return cameraPixels;
    }

    public void setCameraPixels(int cameraPixels) {
        this.cameraPixels = cameraPixels;
    }

    public boolean isDualSIM() {
        return dualSIM;
    }

    public void setDualSIM(boolean dualSIM) {
        this.dualSIM = dualSIM;
    }

    public boolean isFaceUnlock() {
        return faceUnlock;
    }

    public void setFaceUnlock(boolean faceUnlock) {
        this.faceUnlock = faceUnlock;
    }
}
