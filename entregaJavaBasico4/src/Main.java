import com.example.SmartDevice;
import com.example.SmartPhone;
import com.example.SmartWatch;

public class Main {
    public static void main(String[] args) {


        //New SmartDevice
        SmartDevice myDevice = new SmartDevice();
        myDevice.setPrice(122.50);
        myDevice.setAutonomyHours(300);
        myDevice.setWeight(10.23);
        myDevice.setTypeConnectivity("TIPO C");

        System.out.println("*** Print myDevice ***");
        System.out.println("Price:" + myDevice.getPrice());
        System.out.println("AutonomyHours:" + myDevice.getAutonomyHours());
        System.out.println("Weight:" + myDevice.getWeight());
        System.out.println("TypeConnectivity:" + myDevice.getTypeConnectivity());


        //New SmartPhone
        SmartPhone mySmartPhone = new SmartPhone(250, "USB", 1.52, 545.20, 5, 30,true,true);
        System.out.println("*** Print mySmartPhone ***");
        System.out.println("Price:" + mySmartPhone.getPrice());
        System.out.println("AutonomyHours:" + mySmartPhone.getAutonomyHours());
        System.out.println("Weight:" + mySmartPhone.getWeight());
        System.out.println("TypeConnectivity:" + mySmartPhone.getTypeConnectivity());
        System.out.println("ScreenSize:" + mySmartPhone.getScreenSize());
        System.out.println("CameraPixels:" +mySmartPhone.getCameraPixels());
        System.out.println("DualSIM:" + mySmartPhone.isDualSIM());
        System.out.println("FaceUnlock:" + mySmartPhone.isFaceUnlock() );


        //New SmartWatch
        SmartWatch mySmartWatch = new SmartWatch();
        mySmartWatch.setPrice(75.50);
        mySmartWatch.setAutonomyHours(100);
        mySmartWatch.setWeight(0.50);
        mySmartWatch.setTypeConnectivity("TIPO C");
        mySmartWatch.setHeartRateMonitors(true);
        mySmartWatch.setSleepMonitor(false);
        mySmartWatch.setMessaging(true);
        mySmartWatch.setInternalMemory(500);

        System.out.println("*** Print mySmartWatch ***");
        System.out.println("Price:" + mySmartWatch.getPrice());
        System.out.println("AutonomyHours:" + mySmartWatch.getAutonomyHours());
        System.out.println("Weight:" + mySmartWatch.getWeight());
        System.out.println("TypeConnectivity:" + mySmartWatch.getTypeConnectivity());
        System.out.println("HeartRateMonitors:" + mySmartWatch.isHeartRateMonitors());
        System.out.println("SleepMonitor:" +mySmartWatch.isSleepMonitor());
        System.out.println("Messaging:" + mySmartWatch.isMessaging());
        System.out.println("InternalMemory:" + mySmartWatch.getInternalMemory());


    }
}