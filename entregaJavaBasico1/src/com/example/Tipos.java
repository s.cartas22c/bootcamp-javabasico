package com.example;

public class Tipos {

    public static void main(String[] args) {

        //1.Numericos

        //1.1 Enteros

        byte variable1 = 5;
        System.out.println("byte:"+ variable1);

        short variable2 = 10;
        System.out.println("short:"+ variable2);

        int variable3 = 30;
        System.out.println("int:"+ variable3);

        long variable4 = 100;
        System.out.println("long:"+ variable4);

        long variable5 = 6566565;
        System.out.println("long:"+ variable5);

        //1.1 Decimales
        float variable6 = 5.5f;
        System.out.println("float:"+ variable6);

        double variable7 = 1.5d;
        System.out.println("double:"+ variable7);


        //2 Booleano
        boolean variable8 = true;
        System.out.println("boolean true:"+ variable8);

        boolean variable9 = false;
        System.out.println("boolean false:"+ variable9);


        //3 Texto
        char variable10 = 'C';
        System.out.println("char:"+ variable10);

        String variable11 = "Esto es una cadena de texto";
        System.out.println("String:"+ variable11);

    }
}