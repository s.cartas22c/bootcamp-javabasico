package implementacion;

import interfaces.CocheCRUD;

public class CocheCRUDImpl implements CocheCRUD {


    @Override
    public void save() {
        System.out.println("Method save was called");
    }

    @Override
    public void findAll() {
        System.out.println("Method findAll was called");

    }

    @Override
    public void delete() {
        System.out.println("Method delete was called");
    }
}
