import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.*;
import java.util.stream.Collectors;

public class Main {


    public static String reverse(String texto) {

        StringBuilder mybox = new StringBuilder();
        mybox.append(texto);
        return mybox.reverse().toString();
    }

   /* Escribe el código que devuelva una cadena al revés. Por ejemplo, la cadena "hola mundo", debe retornar "odnum aloh".
    1.Crea un array unidimensional de Strings y recórrelo, mostrando únicamente sus valores.
    1.Crea un array bidimensional de enteros y recórrelo, mostrando la posición y el valor de cada elemento en ambas dimensiones.
    2.Crea un "Vector" del tipo de dato que prefieras, y añádele 5 elementos. Elimina el 2o y 3er elemento y muestra el resultado final.
    3.Indica cuál es el problema de utilizar un Vector con la capacidad por defecto si tuviésemos 1000 elementos para ser añadidos al mismo.
    4.Crea un ArrayList de tipo String, con 4 elementos. Cópialo en una LinkedList. Recorre ambos mostrando únicamente el valor de cada elemento.
    5.Crea un ArrayList de tipo int, y, utilizando un bucle rellénalo con elementos 1..10. A continuación, con otro bucle, recórrelo y elimina los numeros pares. Por último, vuelve a recorrerlo y muestra el ArrayList final. Si te atreves, puedes hacerlo en menos pasos, siempre y cuando cumplas el primer "for" de relleno.
    6.Crea una función DividePorCero. Esta, debe generar una excepción ("throws") a su llamante del tipo ArithmeticException que será capturada por su llamante (desde "main", por ejemplo). Si se dispara la excepción, mostraremos el mensaje "Esto no puede hacerse". Finalmente, mostraremos en cualquier caso: "Demo de código".
    7.Utilizando InputStream y PrintStream, crea una función que reciba dos parámetros: "fileIn" y "fileOut". La tarea de la función será realizar la copia del fichero dado en el parámetro "fileIn" al fichero dado en "fileOut".
    8.Sorpréndenos creando un programa de tu elección que utilice InputStream, PrintStream, excepciones, un HashMap y un ArrayList, LinkedList o array.
    */


    public static void main(String[] args) {

        //Call method reverse
        System.out.println(reverse("hola mundo"));

        //Crea un array unidimensional de Strings y recórrelo, mostrando únicamente sus valores.
        String[] myUnidimensionalArray = {"Azul", "Amarillo", "Rojo", "Blanco"};

        for (String color : myUnidimensionalArray) {
            System.out.println("The color is:" + color);
        }


        //Crea un array bidimensional de enteros y recórrelo, mostrando la posición y el valor de cada elemento en ambas dimensiones.
        int[][] myBidimensionalArray = new int[2][1];

        myBidimensionalArray[0][0] = 1;
        myBidimensionalArray[1][0] = 3;


        for (int i = 0; i < myBidimensionalArray.length; i++) {
            for (int j = 0; j < myBidimensionalArray[0].length; j++) {
                System.out.println(myBidimensionalArray[i][j]);
            }
        }

        //Crea un "Vector" del tipo de dato que prefieras, y añádele 5 elementos.
        //Elimina el 2o y 3er elemento y muestra el resultado final.

        Vector<Integer> myVector = new Vector<>();

        myVector.add(5);
        myVector.add(10);
        myVector.add(40);
        myVector.add(15);
        myVector.add(32);
        System.out.println(myVector);

        myVector.remove(1);
        myVector.remove(1);
        System.out.println("MyVector:" + myVector);


        //Indica cuál es el problema de utilizar un Vector
        //con la capacidad por defecto si tuviésemos 1000 elementos para ser añadidos al mismo.

        /*
        A nivel de espacio no existe problema ya que el Vector es un array dinamico, cuya capacidad varia dinamicamente en funcion
        del numero de elementos que estemos introduciendo.
         Si es cierto, que si nuestro array inicialmente era de capacidad 400 y se quieren insertar 10000 extras,
         la operación es costosa, ya que se tiene que generar un nuevo array con la capacidad mayor para soportar todos los elementos
         y copiar el primer array a este segundo. Computacionalmente es una operacion costosa.
        */

        //Crea un ArrayList de tipo String, con 4 elementos. Cópialo en una LinkedList.
        //Recorre ambos mostrando únicamente el valor de cada elemento.

        ArrayList<String> arrayStrings = new ArrayList<>();
        arrayStrings.add("Manzana");
        arrayStrings.add("Pera");
        arrayStrings.add("Melon");
        arrayStrings.add("Sandia");

        for (String fruta : arrayStrings) {
            System.out.println(fruta);
        }

        LinkedList<String> myLinkedList = new LinkedList<>(arrayStrings);

        System.out.println("Print myLinkedList");
        while (myLinkedList.iterator().hasNext()) {
            System.out.println(myLinkedList.iterator().next());
            myLinkedList.remove();
        }


        /* Crea un ArrayList de tipo int, y, utilizando un bucle rellénalo con elementos 1..10.
         A continuación, con otro bucle, recórrelo y elimina los numeros pares.
         Por último, vuelve a recorrerlo y muestra el ArrayList final. Si te atreves, puedes hacerlo en menos pasos,
         siempre y cuando cumplas el primer "for" de relleno.
        */


        ArrayList<Integer> myArrayListInt = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            myArrayListInt.add(i);
        }

        List<Integer> listPares = myArrayListInt.stream().filter(p -> p % 2 == 0).toList();
        myArrayListInt.removeAll(listPares);
        System.out.println("MyArrayListIntFinal" + myArrayListInt);


        /* Crea una función DividePorCero. Esta, debe generar una excepción ("throws")
         a su llamante del tipo ArithmeticException que será capturada por su llamante (desde "main", por ejemplo).
         Si se dispara la excepción, mostraremos el mensaje "Esto no puede hacerse".
        Finalmente, mostraremos en cualquier caso: "Demo de código".*/

        try {
            System.out.println(dividePorCero(5));
        } catch (ArithmeticException e) {
            System.out.println("Eso no puede hacerse");
        } finally {
            System.out.println("Demo de codigo");
        }


        //Utilizando InputStream y PrintStream, crea una función que reciba dos parámetros: "fileIn" y "fileOut".
        // La tarea de la función será realizar la copia del fichero dado en el parámetro "fileIn" al fichero dado en "fileOut".

        try{
            copyFile("prueba.txt", "copiaPrueba.txt");
        }
        catch (Exception e){
            System.out.println("Se ha producido un error al copiar el fichero");
        }
        finally {
            System.out.println("Bloque try/catch completado");
        }

        //Sorpréndenos creando un programa de tu elección que utilice InputStream, PrintStream, excepciones,
        // un HashMap y un ArrayList, LinkedList o array.
        try {
            extractedEmails("prueba.txt", "listaEmails.txt");
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        finally {
            System.out.println("Bloque try/catch completado");
        }


    }

    private static int dividePorCero(int number) {
        return number / 0;
    }

    private static void copyFile(String fileIn, String fileOut) throws IOException {

        try (
                InputStream fis = new FileInputStream("src/resources/" + fileIn);
                InputStreamReader isr = new InputStreamReader(fis,
                        StandardCharsets.UTF_8);
                BufferedReader br = new BufferedReader(isr)) {

             PrintStream myPrint = new PrintStream("src/resources/" + fileOut);

             br.lines().forEach(p ->{myPrint.print(p);
                 myPrint.print(System.lineSeparator());});             myPrint.close();
        }

    }


    private static void extractedEmails(String fileIn, String fileOut) throws IOException {


        try (
                InputStream fis = new FileInputStream("src/resources/" + fileIn);
                InputStreamReader isr = new InputStreamReader(fis,
                        StandardCharsets.UTF_8);
                BufferedReader br = new BufferedReader(isr)) {

            HashMap<Integer, List<String>> reportEmails = new HashMap<Integer, List<String>>();

            List<String> listLines = br.lines().toList();

            //Scrap each line and catch emails.
            int i = 1;
            for (String linea : listLines) {
                String[] splitLine = linea.split(" ");
                reportEmails.put(i, Arrays.stream(splitLine).filter(p -> p.contains("@")).collect(Collectors.toList()));
                i++;
            }

            //Create output file with hashmap information.
            PrintStream myPrint = new PrintStream("src/resources/" + fileOut);
            for (Map.Entry<Integer, List<String>> entry : reportEmails.entrySet()) {
                 if (entry.getValue().size() > 0) {
                    myPrint.print("Line Number:" + entry.getKey());
                    myPrint.print(System.lineSeparator());
                    myPrint.print("List of emails:" + entry.getValue().toString());
                    myPrint.print(System.lineSeparator());

                }
            }
            myPrint.close();

        }
    }


}